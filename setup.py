from setuptools import convert_path, setup


def load_package_meta():
    meta_path = convert_path('./pythondev/__meta__.py')
    meta_ns = {}
    with open(meta_path) as f:
        exec(f.read(), meta_ns)
    return meta_ns['DATA']


PKG_META = load_package_meta()

setup(
    keywords=['pythondev'],
    packages=['pythondev'],
    package_dir={
        'pythondev': 'pythondev'
    },
    zip_safe=False,
    setup_requires=[
        'flake8',
        'pytest-runner',
    ],
    author=PKG_META['author'],
    author_email=PKG_META['author_email'],
    description=PKG_META['description'],
    license=PKG_META['license'],
    name=PKG_META['name'],
    url=PKG_META['url'],
    version=PKG_META['version'],
    tests_require=['pytest', ],
)
