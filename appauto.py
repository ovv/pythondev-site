# -*- coding: utf-8 -*-
"""Create an application instance."""
from flask.helpers import get_debug_flag

from pythondev.app import create_app
from pythondev.settings import DevConfig, ProdConfig


CONFIG = DevConfig if get_debug_flag() else ProdConfig
application = create_app(CONFIG)

if __name__ == '__main__':
    application.run()

