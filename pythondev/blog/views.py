"""Views for the site blog"""
from flask import Blueprint, render_template

blueprint = Blueprint('blog', __name__, url_prefix='/blog', static_folder='../static')


@blueprint.route('/')
def blog():
    return render_template('blog/index.html')
