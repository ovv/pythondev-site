from logging.handlers import RotatingFileHandler

from flask import Flask, render_template

from . import blog, commands, main, users
from .extensions import csrf_protect, db, debug_toolbar, migrate, security


def create_app(config_object):
    """An application factory, as explained here: http://flask.pocoo.org/docs/patterns/appfactories/.
    :param config_object: The configuration object to use.
    """
    app = Flask(__name__.split('.')[0])
    app.config.from_object(config_object)

    register_extensions(app)
    register_commands(app)
    register_blueprints(app)

    handler = RotatingFileHandler(app.config['LOGGING_LOCATION'],
                                  maxBytes=100000, backupCount=1)
    handler.setLevel(app.config['LOGGING_LEVEL'])
    app.logger.addHandler(handler)
    return app


def register_extensions(app):
    db.init_app(app)
    migrate.init_app(app)

    # Flask security config
    security.init_app(app, users.models.user_datastore)
    csrf_protect.init_app(app)

    if app.config['DEBUG']:
        debug_toolbar.init_app(app)

    return None
    
def register_blueprints(app):
    """Register Flask Blueprints."""
    app.register_blueprint(main.views.blueprint)
    app.register_blueprint(blog.views.blueprint)


def register_errorhandlers(app):
    """Register error handlers."""

    def render_error(error):
        """Render error template."""
        # If a HTTPException, pull the `code` attribute; default to 500
        error_code = getattr(error, 'code', 500)
        return render_template('{0}.html'.format(error_code)), error_code

    for errcode in [401, 404, 500]:
        app.errorhandler(errcode)(render_error)
    return None


def register_shellcontext(app):
    """Register shell context objects."""
    def shell_context():
        """Shell context objects."""
        return {
            'db': db}

    app.shell_context_processor(shell_context)

def register_commands(app):
    """Register Click commands."""
    app.cli.add_command(commands.test)
    app.cli.add_command(commands.lint)
    app.cli.add_command(commands.clean)
    app.cli.add_command(commands.urls)
    app.cli.add_command(commands.create_tables)
