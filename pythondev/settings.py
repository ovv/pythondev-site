"""Application configuration."""
import logging
import os


class Config(object):
    """Base configuration."""

    SECRET_KEY = os.environ.get('FLASK_SECRET', 'asdñlajsddasdas1d56a132')
    APP_DIR = os.path.abspath(os.path.dirname(__file__))  # This directory
    PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Flask Security
    SECURITY_PASSWORD_HASH = 'sha512_crypt'
    SECURITY_PASSWORD_SALT = os.environ.get('FLASK_PASSWORD_SALT', 'd56as3d1a5s61d65as165a1s5') # This must be a env var.


class ProdConfig(Config):
    """Production configuration."""

    ENV = 'prod'
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    LOGGING_LEVEL = logging.WARNING
    LOGGING_LOCATION = 'pythondev.log'


class DevConfig(Config):
    """Development configuration."""

    ENV = 'dev'
    DEBUG = True
    DB_NAME = 'dev.db'
    # Put the db file in project root
    DB_PATH = os.path.join(Config.PROJECT_ROOT, DB_NAME)
    SQLALCHEMY_DATABASE_URI = 'sqlite:///{0}'.format(DB_PATH)
    LOGGING_LOCATION = 'pythondev.log'
    LOGGING_LEVEL = logging.DEBUG
